(function ($, Drupal, once) {
  Drupal.behaviors.mediaelement = {
    attach(context, settings) {
      $(once('mediaelement', '.mediaelementjs', context))
        .each(function () {
          $(this).mediaelementplayer(settings.mediaelement);
        });

      // Attach player to other elements if MediaElement.js is set to global.
      if (settings.mediaelement.attachSitewide !== undefined) {
        $(once('mediaelement', 'audio,video', context))
          .each(function () {
            $(this).mediaelementplayer(settings.mediaelement);
          });
      }
    },
  };
})(jQuery, Drupal, once);
